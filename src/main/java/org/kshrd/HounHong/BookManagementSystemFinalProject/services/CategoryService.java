package org.kshrd.HounHong.BookManagementSystemFinalProject.services;

import org.kshrd.HounHong.BookManagementSystemFinalProject.models.Category;

import java.util.List;

public interface CategoryService {
    public List<Category>findCategory();
    public Category finOne(int id);
    public boolean save(Category category);
    public boolean update(Category category);
    public boolean delete(int id);
}
