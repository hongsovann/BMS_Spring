package org.kshrd.HounHong.BookManagementSystemFinalProject.services.impls;

import org.kshrd.HounHong.BookManagementSystemFinalProject.models.Category;
import org.kshrd.HounHong.BookManagementSystemFinalProject.reposities.CategoryRepository;
import org.kshrd.HounHong.BookManagementSystemFinalProject.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl  implements CategoryService{
    private CategoryRepository categoryRepository;
    @Autowired
    CategoryServiceImpl(CategoryRepository categoryRepository){
        this.categoryRepository=categoryRepository;
    }
    @Override
    public List<Category> findCategory() {
        return categoryRepository.findAllCategory();
    }

    @Override
    public Category finOne(int id) {
        return categoryRepository.finOne(id);
    }

    @Override
    public boolean save(Category category) {
        categoryRepository.save(category);
        return false;
    }

    @Override
    public boolean update(Category category) {
        categoryRepository.update(category);
        return false;
    }

    @Override
    public boolean delete(int id) {
        categoryRepository.delete(id);
        return false;
    }
}
