package org.kshrd.HounHong.BookManagementSystemFinalProject.services.impls;

import org.kshrd.HounHong.BookManagementSystemFinalProject.models.Article;
import org.kshrd.HounHong.BookManagementSystemFinalProject.reposities.ArticleReposity;
import org.kshrd.HounHong.BookManagementSystemFinalProject.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServericeImpl implements ArticleService {
    private ArticleReposity articleReposity;
    @Autowired
    ArticleServericeImpl(ArticleReposity articleReposity){
        this.articleReposity=articleReposity;
    }

    @Override
    public List<Article> findAllArtilce() {
        return articleReposity.findAllArticle();
    }

    @Override
    public Boolean save(Article article) {
       articleReposity.saveArticle(article);

        return null;
    }

    @Override
    public Article findArticleOne(String articleHash) {
        return articleReposity.findArticleOne(articleHash);
    }

    @Override
    public boolean delete(String articleHash) {
        articleReposity.delete(articleHash);
        return false;
    }

    @Override
    public boolean update(Article article) {
        articleReposity.update(article);
        return false;
    }
}
