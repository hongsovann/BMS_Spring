package org.kshrd.HounHong.BookManagementSystemFinalProject.services;

import org.kshrd.HounHong.BookManagementSystemFinalProject.models.Article;


import java.util.List;


public interface ArticleService {
    List<Article>findAllArtilce();
    Boolean save(Article article);
    public Article findArticleOne(String articleHash);
    boolean delete(String articleHash);
    public boolean update(Article article);


}
