package org.kshrd.HounHong.BookManagementSystemFinalProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHomewordArticleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringHomewordArticleApplication.class, args);
	}
}
