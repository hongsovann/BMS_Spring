package org.kshrd.HounHong.BookManagementSystemFinalProject.reposities;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.kshrd.HounHong.BookManagementSystemFinalProject.models.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Select("Select * From tbl_categories")
    public List<Category> findAllCategory();
    @Select("select * From tbl_categories WHERE id=#{id}")
    public Category finOne(int id);
    @Insert("Insert into tbl_categories(name) values(#{name})")
    public boolean save(Category category);
    @Update("Update tbl_categories set name=#{name} where id=#{id} ")
    public boolean update(Category category);
    @Delete("DELETE FROM  tbl_categories WHERE id=#{id}")
    public boolean delete(int id);
}
