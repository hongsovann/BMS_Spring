package org.kshrd.HounHong.BookManagementSystemFinalProject.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration

public class FileUploadConfiguration extends WebMvcConfigurerAdapter{

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("article/images/**")
                .addResourceLocations("file:C://Users/HounHong/Desktop/Image/");
    }
}
