package org.kshrd.HounHong.BookManagementSystemFinalProject.configurations;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DataSourcConfiguration {
    @Bean
    @Profile("database")
    @Qualifier("dataSource")
    public DataSource dataSource(){
        DriverManagerDataSource driverManagerDataSource=new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
        driverManagerDataSource.setUrl("jdbc:postgresql://localhost:9090/bookmanagementsystem");
        driverManagerDataSource.setUsername("postgres");
        driverManagerDataSource.setPassword("root");
        return driverManagerDataSource;
    }
    @Bean
    @Profile("memory")
    @Qualifier("dataSource")
    public DataSource developMemory() {
        EmbeddedDatabaseBuilder db=new EmbeddedDatabaseBuilder();
        db.setType(EmbeddedDatabaseType.H2);
        db.addScript("script/table.sql");
        return db.build();
    }
}
