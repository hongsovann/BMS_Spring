package org.kshrd.HounHong.BookManagementSystemFinalProject.configurations;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan("org.kshrd.HounHong.BookManagementSystemFinalProject.reposities")
public class MyBatisConfiguration {

    private DataSource dataSource;
   MyBatisConfiguration(@Qualifier("dataSource") DataSource dataSource1){
       this.dataSource=dataSource1;
   }

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager(){
        return new DataSourceTransactionManager(dataSource);
    }
    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean() {
        SqlSessionFactoryBean sqlf=new SqlSessionFactoryBean();
        sqlf.setDataSource(dataSource);
        return sqlf;
    }

}
