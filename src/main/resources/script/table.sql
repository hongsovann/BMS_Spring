create table tbl_categories(
	id int primary key auto_increment,
	name varchar(100));

	create table tbl_articles(
	id int primary key auto_increment,
	title varchar(50),
	description Text,
	thumbnail varchar(100),
	article_hash varchar(50),
	import_date VARCHAR (100),
	author VARCHAR (50),
	category_id   int references tbl_categories(id)
	ON UPDATE CASCADE
  ON DELETE RESTRICT
);
insert into tbl_categories(name) values(
	'Comedy'
),('Drama'),('Tragedy'),('Legend'),('Novella');